package domain;

public class Company {


    private List<Employee> employees = new ArrayList();

    public Company() {
    }

    public void startWork() {
        this.employees.forEach((e) -> {
            e.work();
        });
    }

    public void addEmployee(Employee employee) {
        this.employees.add(employee);
    }
}


