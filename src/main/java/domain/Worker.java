package domain;

public abstract class Worker implements Employee{
    private String name;

    public Worker(String name) {
        setName(name);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        getName(name);
    }
}
