package domain;

public class Cpp extends Worker implements BackEnd, FrontEnd {

    public Cpp(String name) {
        super(name);
    }

    @Override
    public void writeProgram() {
        System.out.println("I'm creating cpp application");
    }

    @Override
    public void writeFront() {
        System.out.println("I'm write frontend For app");
    }

    @Override
    public void develop() {
        System.out.println("My name is "+getName());
        writeFront();
        writeProgram();
    }

    @Override
    public void work() {
        develop();
    }
}
