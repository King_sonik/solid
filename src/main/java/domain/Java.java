package domain;

public class Java extends Worker implements BackEnd {
    public Java(String name) {
        super(name);

    }

    private void writeJava() {
        System.out.println("Hello"+getName()+"I'm writing Java..");
    }

    @Override
    public void writeProgram() {
      writeJava();
    }

    @Override
    public void develop() {
        writeProgram();
    }

    @Override
    public void work() {
       develop();
    }
}

