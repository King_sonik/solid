package domain;

public class iOS extends Worker implements FrontEnd{

    public iOS(String name) {
        super(name);
    }
    public void WriteIOS(){
        System.out.println("my name is "+getName()+" and I develop iOS");
    }

    @Override
    public void writeFront() {
    WriteIOS();
    }

    @Override
    public void develop() {
        writeFront();
    }

    @Override
    public void work() {
       develop();
    }


}
