package domain;

public interface FrontEnd extends Developer {
    void writeFront();
}
