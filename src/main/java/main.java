import domain.Company;
import domain.Cpp;
import domain.Java;
import domain.iOS;

public class main {
    public static void main(String[] args) {
        Company company = new Company();
        company.addEmployee(new Java("Murat"));
        company.addEmployee(new Cpp("Sultan"));
        company.addEmployee(new iOS("Sonik"));
        company.startWork();
    }

}
